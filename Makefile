USER = gwenaelle
DOCKER_PHP = docker exec -it private-family_php /bin/bash -c
DOCKER_PHP_EXEC = docker exec -u php private-family_php
PHP_CONSOLE = php bin/console
DC = docker compose

install:
	@echo "------------------------------------------------------"
	@echo "Install Development Environment (Docker)"
	@echo "------------------------------------------------------"

	@echo "--> Create migrations, var, cache, logs folders and set users rights"
	if [ ! -d "migrations" ]; then mkdir migrations && chown ${USER}:${USER} migrations; fi
	if [ ! -d "var" ]; then mkdir var && chown ${USER}:${USER} var; fi
	if [ ! -d "var/cache" ]; then mkdir var/cache && chown ${USER}:${USER} var/cache; fi
	if [ ! -d "var/cache/dev" ]; then mkdir var/cache/dev && chown ${USER}:${USER} var/cache/dev; fi
	if [ ! -d "var/log" ]; then mkdir var/log && chown ${USER}:${USER} var/log; fi

	@echo "--> Create containers"
	@${DC} build

	@echo "--> Up containers"
	@${DC} up -d

	@echo "--> Install backend dependencies"
	@${DOCKER_PHP_EXEC} composer install

	@echo "--> Set Node version 18.9.0"
	nvm use v18.9.0

	@echo "--> Install frontend dependencies"
	npm install

	@echo "--> Run webpack dev"
	npm run dev

php:
	${DOCKER_PHP} "export TERM=xterm-256color; exec bash;"

start:
	@${DC} start

assets-dev:
	npm run dev

assets-watch:
	npm run watch

composer-update:
	@${DOCKER_PHP_EXEC} composer update

doctrine-migrate:
	@${DOCKER_PHP_EXEC} ${PHP_CONSOLE} doctrine:cache:clear-query
	@${DOCKER_PHP_EXEC} ${PHP_CONSOLE} doctrine:migrations:diff
	@${DOCKER_PHP_EXEC} ${PHP_CONSOLE} doctrine:migrations:migrate

cc:
	@${DOCKER_PHP_EXEC} ${PHP_CONSOLE} cache:clear

update-gitignore:
	git rm -r --cached .
	git add .
	git commit -m ".gitignore update"
	git push

prod-get-composer:
	curl -sS https://getcomposer.org/installer | php
	php composer.phar install

prod-composer:
	php composer.phar install

docker-configuration:
	cp documentation/config/docker/doctrine.docker.yaml config/packages/doctrine.yaml;
	npm run dev

prod-configuration:
	cp documentation/config/prod/doctrine.yaml config/packages/doctrine.yaml;
	npm run build
	git add .
	git commit -m "prod configuration"
	git push

prod-hotfix:
	cp documentation/config/prod/doctrine.yaml config/packages/doctrine.yaml;
	npm run build
	git add .
	git commit -m "prod hotfix"

hotfix-configuration:
	cp documentation/config/prod/doctrine.yaml config/packages/doctrine.yaml;
	npm run build
	git add .
	git commit -m "hotfix"