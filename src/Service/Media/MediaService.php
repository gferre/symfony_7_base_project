<?php

namespace App\Service\Media;

use App\Entity\Media\Media;
use App\Repository\Media\MediaRepository;
use App\Service\Core\FileService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

readonly class MediaService
{

    public function __construct(private MediaRepository       $repository,
                                private ParameterBagInterface $bag, private FileService $fileService)
    {
    }

    public function findAll()
    {
        return $this->repository->findBy([], ['name' => 'ASC']);
    }

    public function findByCode(string $code): ?Media
    {
        return $this->repository->findOneBy(['code' => $code]);
    }

    public function create(string $tmpPath, string $name, string $extension): void
    {
        $media = new Media();
        $media->setName($name);
        $media->setExtension($extension);
        $media->setType('video');
        $media->setCode(uniqid());

        $filename = sprintf('%s.%s', $media->getName(), $media->getExtension());
        $media->setPrivatePath(sprintf('%s/%s', $this->bag->get('medias_dir_private'), $filename));
        $media->setPublicPath(sprintf('%s/%s', $this->bag->get('medias_dir_public'), $filename));
        move_uploaded_file($tmpPath, $media->getPrivatePath());
        $media->setSize($this->fileService->getFileSize($media->getPrivatePath()));

        $this->repository->save($media);
    }


}