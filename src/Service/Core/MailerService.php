<?php

namespace App\Service\Core;

use App\Entity\Core\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;

readonly class MailerService
{
    private const MAIL_PASSWORD_RESETTING = 'mailing/password_reset.html.twig';
    private const MAIL_EMAIL_VALIDATION = 'mailing/mail_validation.html.twig';

    private MailerInterface $mailer;
    private string $adminMail;

    /**
     * @param MailerInterface       $mailer
     * @param ParameterBagInterface $bag
     */
    public function __construct(MailerInterface $mailer,
                                ParameterBagInterface $bag)
    {
        $this->mailer = $mailer;
        $this->adminMail = $bag->get('admin_mail');
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendPasswordResetEmail(UserInterface|User $user, ResetPasswordToken $resetToken): void
    {
        $params = ['resetToken' => $resetToken];

        $this->sendMail(
            $user->getEmail(),
            'Private Family - Régénération de votre mot de passe',
            self::MAIL_PASSWORD_RESETTING,
            $params);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendEmailValidation(string $email, array $params): void
    {
        $this->sendMail(
            $email,
            'MeetaTôonz - Validation de votre adresse mail',
            self::MAIL_EMAIL_VALIDATION,
            $params);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendMail(string $to, string $subject, string $template, array $params): void
    {
        $email = (new TemplatedEmail())
            ->from($this->adminMail)
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($params);

        $this->mailer->send($email);
    }

}