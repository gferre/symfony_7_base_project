<?php

namespace App\Service\Core;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FileService
{
    private ParameterBagInterface $bag;

    /**
     * @param ParameterBagInterface $bag
     */
    public function __construct(ParameterBagInterface $bag)
    {
        $this->bag = $bag;
    }

    /**
     * @param string $path
     * @return void
     */
    public function createFolderIfNotExists(string $path): void
    {
        if(!file_exists($path)) mkdir($path);
    }

    /**
     * @param string $path
     *
     * @return false|int|null
     */
    public function getFileSize(string $path): false|int|null
    {
        if(file_exists($path)) return filesize($path);

        return null;
    }

    /**
     * @param string $path
     *
     * @return void
     */
    public function deleteFile(string $path): void
    {
        if(file_exists($path)) unlink($path);
    }

    /**
     * @param             $file
     * @param string      $extension
     * @param array       $subFolders
     *
     * @return string
     */
    public function tmpToUploads($file, string $code, string $extension, array $subFolders = []): string
    {
        $privatePath = $this->bag->get('public_dir');
        $this->createFolderIfNotExists($privatePath);

        foreach ($subFolders as $subFolder) {
            $privatePath .= '/' . $subFolder;
            $this->createFolderIfNotExists($privatePath);
        }

        $newFilename = sprintf('%s.%s', $code, $extension);
        $filePath = sprintf('%s/%s', $privatePath, $newFilename);
        move_uploaded_file($file, $filePath);

        return sprintf('%s/%s', $privatePath, $newFilename);
    }

    /**
     * @return string
     */
    private function getPublicDir(): string
    {
        return $this->bag->get('public_dir');
    }

}