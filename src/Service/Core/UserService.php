<?php

namespace App\Service\Core;

use App\Entity\Core\User;
use App\Repository\Core\UserRepository;
use DateTimeImmutable;

readonly class UserService
{
    public function __construct(private SecurityService $securityService,
                                private UserRepository  $repository)
    {
    }

    public function findAll(): array
    {
        return $this->repository->findBy([]);
    }

    public function save(User $user, string $password, array $roles = ['ROLE_USER']): User
    {
        $passwordHashed = $this->securityService->hashPassword($user, $password);
        $user->setPassword($passwordHashed);
        $user->setRoles($roles);

        $this->repository->save($user);

        return $user;
    }

    public function edit(User $user): void
    {
        $user->setUpdatedAt(new DateTimeImmutable());
        $this->repository->save($user);
    }

    public function delete(User $user): void
    {
        $this->repository->delete($user);
    }

    public function findByUsername(string $username): ?User
    {
        return $this->repository->findOneBy(['username' => $username]);
    }

    public function findByEmail(string $email): ?User
    {
        return $this->repository->findOneBy(['email' => $email]);
    }

}