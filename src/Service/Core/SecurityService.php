<?php

namespace App\Service\Core;

use App\Entity\Core\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

readonly class SecurityService
{
    public function __construct(private ResetPasswordHelperInterface $resetPasswordHelper,
                                private UserPasswordHasherInterface  $passwordHasher,
                                private VerifyEmailHelperInterface   $verifyEmailHelper,
                                private MailerService                $mailerService,
                                private EntityManagerInterface       $em)
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendEmailConfirmation(string $verifyEmailRouteName, UserInterface|User $user): void
    {
        $signatureComponents = $this->verifyEmailHelper->generateSignature(
            $verifyEmailRouteName,
            $user->getId(),
            $user->getEmail()
        );

        $params = [];
        $params['signedUrl'] = $signatureComponents->getSignedUrl();
        $params['expiresAtMessageKey'] = $signatureComponents->getExpirationMessageKey();
        $params['expiresAtMessageData'] = $signatureComponents->getExpirationMessageData();

        $this->mailerService->sendEmailValidation($user->getEmail(), $params);
    }

    public function handleEmailConfirmation(Request $request, UserInterface|User $user): void
    {
        $this->verifyEmailHelper->validateEmailConfirmationFromRequest($request, $user->getId(), $user->getEmail());

        $user->setVerified(true);

        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function processSendingPasswordResetEmail(string $emailFormData): ResetPasswordToken
    {
        $user = $this->em->getRepository(User::class)->findOneBy([
            'email' => $emailFormData,
        ]);

        if (!$user) {
            throw new Exception('Utilisateur non trouvé.');
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            throw new Exception($e->getReason());
        }

        $this->mailerService->sendPasswordResetEmail($user,$resetToken);

        return $resetToken;
    }

    public function resetPassword(string $token, UserInterface|User $user, string $password): void
    {
        // A password reset token should be used only once, remove it.
        $this->resetPasswordHelper->removeResetRequest($token);

        // Encode(hash) the plain password, and set it.
        $encodedPassword = $this->hashPassword($user, $password);

        $user->setPassword($encodedPassword);
        $user->setUpdatedAt(new DateTimeImmutable());
        $user->setPasswordHasToBeReset(false);
        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * @param User   $user
     * @param string $password
     *
     * @return string
     */
    public function hashPassword(User $user, string $password): string
    {
        return $this->passwordHasher->hashPassword($user, $password);
    }

}