<?php

namespace App\Command;

use App\Entity\Media\Media;
use App\Service\Core\FileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

#[AsCommand(
    name: 'app:init',
    description: 'Initialisation des données.',
    hidden: false
)]
class initCommand extends Command
{
    public function __construct(private readonly EntityManagerInterface         $em,
                                private readonly ParameterBagInterface          $bag,
                                private readonly FileService                    $fileService,
                                private readonly PasswordHasherFactoryInterface $factory)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $media = new Media();
        $media->setName('Godzilla Minus One 2023 VF');
        $media->setCode('67700866c0283');
        $media->setExtension('mp4');
        $media->setType('video');
        $filename = sprintf('%s.%s', $media->getCode(), $media->getExtension());
        $media->setPrivatePath(sprintf('%s/%s', $this->bag->get('medias_dir_private'), $filename));
        $media->setPublicPath(sprintf('/%s/%s', $this->bag->get('medias_dir_public'), $filename));
        $media->setSize($this->fileService->getFileSize($media->getPrivatePath()));
        $media->setPassword('godzilla28eliot');

        $this->em->persist($media);
        $this->em->flush();

        return Command::SUCCESS;
    }

}