<?php

namespace App\Controller\Security;

use App\Entity\Core\User;
use App\Form\RegisterType;
use App\Service\Core\SecurityService;
use App\Service\Core\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;

class LogupController extends AbstractController
{
    private const REGISTER_SUCCESS = "Votre compte a bien été créé! Vous pouvez vous connecter.";

    public function __construct(private readonly Security           $security,
                                private readonly UserService        $userService,
                                private readonly SecurityService    $securityService)
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/logup', name: 'app_logup')]
    public function register(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('password')->getData();
            $user = $this->userService->save($user, $password);

            $this->addFlash('success', self::REGISTER_SUCCESS);

            $this->securityService->sendEmailConfirmation('app_verify_email', $user);

            return $this->security->login($user, 'form_login', 'main');
        }

        if($form->isSubmitted() && !$form->isValid()) {
            foreach ($form->getErrors() as $error) {
                $this->addFlash('error', $error);
            }
        }

        return $this->render('security/logup.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->securityService->handleEmailConfirmation($request, $this->getUser());
        } catch (Exception $exception) {
            $this->addFlash('verify_email_error', $exception->getMessage());

            return $this->redirectToRoute('app_logup');
        }

        $this->addFlash('success', 'Votre email a bien été vérifié.');

        return $this->redirectToRoute('app_home');
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/resend/email/validation', name: 'app_resend_mail_validation')]
    public function resendMailValidation(): Response
    {
        $this->securityService->sendEmailConfirmation('app_verify_email', $this->getUser());

        return new JsonResponse('OK');
    }
}