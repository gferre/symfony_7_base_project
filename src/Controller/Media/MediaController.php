<?php

namespace App\Controller\Media;

use App\Service\Media\MediaService;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/media', name: 'app_media_')]
class MediaController extends AbstractController
{
    public function __construct(private readonly MediaService $mediaService)
    {
    }

    #[Route('/list', name: 'list')]
    #[Template('medias/index.html.twig')]
    public function list(): array
    {
        return [
            'medias' => $this->mediaService->findAll(),
        ];
    }

    #[Route('/load/{code}', name: 'load')]
    #[Template('medias/load.html.twig')]
    public function load(string $code): array
    {
        $media = $this->mediaService->findByCode($code);

        return [
            'path' => $media->getPublicPath(),
            'type' => $media->getType() . '/' . $media->getExtension(),
        ];
    }

    #[Route('/upload', name: 'upload')]
    public function upload(): JsonResponse
    {
        $file = $_FILES['file']['tmp_name'];
        $filename = explode('.', $_FILES['file']['name'])[0];
        $extension = explode('.', $_FILES['file']['name'])[1];

        $this->mediaService->create($file, $filename, $extension);

        return new JsonResponse($file);
    }
}