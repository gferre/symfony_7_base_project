<?php

namespace App\Utils;

use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Symfony\Component\Form\FormInterface;

class DateUtils
{
    public const INTERVAL_DAY = 'D';

    public static function getDateTimeFromFormField(FormInterface $form, string $fieldName, string $format = 'd/m/Y'): DateTimeImmutable|bool
    {
        $date = $form->get($fieldName)->getData();
        return DateUtils::getDateTimeFromString($date, 'd/m/Y');
    }

    public static function getDateTodayFormatted(): bool|string
    {
        $today = new DateTimeImmutable("now");

        return date_format($today, 'd/m/Y');
    }

    public static function getStringFromDateTime(DateTimeImmutable $date, string $format = null): bool|string
    {
        if(null === $format) $format = "d/m/Y";

        return date_format($date, $format);
    }

    public static function getDateTimeFromString(string $date, string $format): DateTimeImmutable|bool
    {
        return DateTimeImmutable::createFromFormat($format, $date);
    }

    /**
     * @throws Exception
     */
    public static function addIntervalToDateTime(DateTimeImmutable $datetime,
                                                 int $interval,
                                                 string $intervalTag): DateTimeImmutable
    {
        $plusInterval = clone $datetime;
        $intervalSpec = sprintf('P%s%s', $interval, $intervalTag);

        return $plusInterval->add(new DateInterval($intervalSpec));
    }

    public static function getMonthsDiff(DateTimeImmutable $origin, DateTimeImmutable $target): int
    {
        return $origin->diff($target)->m;
    }

    public static function getDaysDiff(DateTimeImmutable $origin, DateTimeImmutable $target): bool|int
    {
        return $origin->diff($target)->days;
    }

    public static function getHoursDiff(DateTimeImmutable $origin, DateTimeImmutable $target): int
    {
        return $origin->diff($target)->h;
    }

    public static function getMinutesDiff(DateTimeImmutable $origin, DateTimeImmutable $target): int
    {
        return $origin->diff($target)->i;
    }

    public static function getSecondsDiff(DateTimeImmutable $origin, DateTimeImmutable $target): int
    {
        return $origin->diff($target)->s;
    }

    /**
     * @throws Exception
     */
    public static function getDateTimeFromTimestamp(int $timestamp): DateTimeImmutable
    {
        $date = new DateTimeImmutable();

        return $date->setTimestamp($timestamp);
    }

    /**
     * @throws Exception
     */
    public static function getDateTimeFromTrial(int $timestamp): DateTimeImmutable
    {
        $date = new DateTimeImmutable();
        $date->add(new DateInterval($timestamp));

        return $date;
    }

    public static function getDateTimeZone(DateTimeImmutable $dateTime): bool|string
    {
        $datetimezone = new DateTimeZone('GMT');
        $dateTime->setTimezone($datetimezone);
        $date = $dateTime->format('Y-m-d');
        $timestamp = strtotime($date);

        return date('c', $timestamp);
    }

    /**
     *
     * @throws Exception
     */
    public static function checkIfDateTimeIntervalSupAtNow(DateTimeImmutable $dateTime, string $validityTime): bool
    {
        $now = new DateTimeImmutable();
        $dateTime->add(new DateInterval($validityTime));

        return $now > $dateTime;
    }

    public static function isPassed(DateTimeImmutable $date): bool
    {
        $today = new DateTimeImmutable();

        return $date->getTimestamp() <= $today->getTimestamp();
    }
}