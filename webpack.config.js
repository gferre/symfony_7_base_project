const Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    // JS
    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/security', './assets/js/security.js')
    .addEntry('js/medias', './assets/js/medias.js')

    // IMAGES, VIDEOS, ETC
    // .copyFiles({
    //     from:'./assets/medias/images',
    //     to: 'images/[path][name].[ext]'})

    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .autoProvidejQuery()
    .enableSassLoader()
;

module.exports = Encore.getWebpackConfig();
