import $ from "jquery";

// https://github.com/danielm/uploader
require('../../../node_modules/dm-file-uploader/dist/js/jquery.dm-uploader.min.js');

export default class DmUploader {

    static init(uploader,
                url,
                onUploadSuccess = null,
                onUploadComplete = null,
                multiple = true) {

        $(uploader).dmUploader({ //
            url: url,
            multiple: multiple,
            // maxFileSize: 3000000, // 3 Megs
            onDragEnter: function(){
                // Happens when dragging something over the DnD area
                this.addClass('active');
            },
            onDragLeave: function(){
                // Happens when dragging something OUT of the DnD area
                this.removeClass('active');
            },
            onInit: function(){
                // Plugin is ready to use
            },
            onComplete: function(id, data){
                // All files in the queue are processed (success or error)
                if(onUploadComplete) onUploadComplete(data);
            },
            onNewFile: function(id, file){
                // When a new file is added using the file selector or the DnD area
            },
            onBeforeUpload: function(id){
                // about tho start uploading a file
            },
            onUploadCanceled: function(id) {
                // Happens when a file is directly canceled by the user.
            },
            onUploadProgress: function(id, percent){
                $('#div_progress').removeClass('d-none')
                $('#div_progress').attr('aria-valuenow', percent)
                $('#progress_bar').css('width', percent+'%')
                $('#progress_bar').html(percent+'%')
            },
            onUploadSuccess: function(id, data){
                // A file was successfully uploaded
                if(onUploadSuccess) onUploadSuccess(data);
            },
            onUploadError: function(id, xhr, status, message){
            },
            onFallbackMode: function(){
            },
            onFileSizeError: function(file){
            }
        });
    }

}

