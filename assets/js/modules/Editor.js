/**
 * Custom module for WYSIWYG TinyMCE text editor
 * https://www.tiny.cloud/docs/tinymce/6/
 */

export function initEditor(textarea, divToDisplay, imagesUploadUrl, data) {
    tinymce.init({
        selector: 'textarea' + textarea,
        language: 'fr_FR',
        menubar: false,
        statusbar: false,
        plugins: 'media image link lists emoticons',
        toolbar: 'undo redo | styles | numlist bullist | bold italic underline |fontsize | alignleft aligncenter alignright alignjustify | link media image emoticons',
        images_upload_url: imagesUploadUrl,
        setup: function (editor) {
            editor.on('init', function (e) {
                if (divToDisplay) {
                    $(divToDisplay).removeClass('d-none')
                    $('#spinner').addClass('d-none')
                }
                if (null !== data) {
                    setDataEditor(textarea, data)
                }
            })
        }
    })
}

export function setDataEditor(textarea, data) {
    tinymce.activeEditor.selection.setContent(data);
}
export function getDataEditor(textarea) {
    return tinymce.get(textarea).getContent();
}

export function destroyEditor(textarea) {
    tinymce.remove('#' + textarea);
}