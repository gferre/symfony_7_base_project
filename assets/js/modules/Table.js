import {alertTypes, successAlert} from "./SweetAlert";
import {callAjax, callAjaxModalForm, clickOnAjax} from "./Ajax";

export function initTable(conf, callbackDone, callbackShow, callbackDoneSubmit) {
    let initEventsTable = function() {

        let callbackSubmit = function(data) {
            $(conf.content).html(data)
            initEventsTable()
            successAlert('Mise à jour effectuée avec succés.')
            if (callbackDoneSubmit) {
                callbackDoneSubmit()
            }
        }

        callAjaxModalForm(
            conf.modalForm,
            [conf.btnCreate, conf.btnEdit],
            callbackShow,
            conf.submitButton,
            conf.form,
            callbackSubmit
        )

        clickOnAjax(conf.btnOnOff, conf.content, initEventsTable, alertTypes.success,
            'Mise à jour effectuée avec succés.')

        clickOnAjax(conf.btnDelete, conf.content, initEventsTable, alertTypes.error,
            'Suppression effectuée avec succés.')

        if (callbackDone) {
            callbackDone()
        }
    }

    initEventsTable()
}


export function initSortTable(tableBody, callbackdone) {
    let conf = {
        btnOrder: '.btn-order',
        iZA: 'fa-arrow-up-z-a',
        iAZ: 'fa-arrow-down-a-z',
    }

    $(conf.btnOrder).off('click').on('click', function(e) {
        e.preventDefault();
        let url = $(this).attr('href');
        let order = $(this).data('order');
        let asc = $(this).data('asc');

        $(conf.btnOrder).data('asc', 'asc');
        $('.icon-sort').removeClass(conf.iZA).addClass(conf.iAZ);

        if (asc === 'asc') {
            $('.icon-'+order).removeClass(conf.iZA).addClass(conf.iAZ);
            $(this).data('asc', 'asc');
        } else {
            $('.icon-'+order).removeClass(conf.iAZ).addClass(conf.iZA);
            $(this).data('asc', 'desc');
        }
        $(this).data('asc', asc === 'asc' ? 'desc' : 'asc');

        url += '?order_by=' + order + '&asc=' + asc;

        let callback = function(data) {
            $(tableBody).html(data);
            callbackdone();
        }
        callAjax(url, 'GET', 'HTML', null, callback);
    })
}