/**
 * The jQuery replacement for select boxes
 * https://select2.org/
 */

export function initSelect2(select, options, add = true) {

    $(options).each(function(key, data) {
        let option = $("<option></option>").val(data.id);
        $(option).text(data.text);
        if(data.selected) {
            $(option).attr('selected', 'selected');
        }
        $(select).append(option);
    })

    $(select).select2({
        tags: add,
    });
}

export function getDataSelect2(select) {
    return $(select).select2('data');
}