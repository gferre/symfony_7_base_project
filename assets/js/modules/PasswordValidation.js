export function checkPasswordValidation() {
    let pwdInputOne = $('#_password_one')
    let pwdInputTwo = $('#_password_two')
    let progressBar = $('#password_security_progress')

    let min = new RegExp(/[A-Z]/)
    let maj = new RegExp(/[a-z]/)
    let number = new RegExp(/[0-9]/)

    function testInput(input, secondInput) {
        $(input).on('input', function() {
            let val = input.val()
            let pourcent = 0;

            if (val.length > 8) {
                pourcent += 20
            }

            if (min.test(val)) {
                pourcent += 20
            }

            if (maj.test(val)) {
                pourcent += 20
            }

            if (number.test(val)) {
                pourcent += 20
            }

            if (input.val() === secondInput.val() && input.val().length > 0 && secondInput.val().length > 0) {
                console.log('les mêmes')
                pourcent += 20
            }

            progressBar.attr('aria-valuenow', pourcent)
            progressBar.find('.progress-bar').css('width', pourcent + '%')
            if (pourcent === 0) {
                progressBar.find('.progress-bar').removeClass('bg-success').removeClass('bg-danger')
            } else if (pourcent > 0 && pourcent < 100) {
                progressBar.find('.progress-bar').removeClass('bg-success').addClass('bg-danger')
            } else if (pourcent === 100) {
                progressBar.find('.progress-bar').removeClass('bg-danger').addClass('bg-success')
            }
        })
    }

    testInput(pwdInputOne, pwdInputTwo)
    testInput(pwdInputTwo, pwdInputOne)
}

export function showPassword() {
    $('.show-password').on('click', function(e) {
        e.preventDefault()
        let target = $(this).data('target')
        if($(target).attr('type') === 'password') {
            $(target).attr('type', 'text')
            $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash')
        } else if ($(target).attr('type') === 'text') {
            $(target).attr('type', 'password')
            $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye')
        }
    })
}