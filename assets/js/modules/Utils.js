import {callAjax, callAjaxForm} from "./Ajax";

export let initFormSubmit = function(btnSubmit, form, callback, modal = null) {
    $(btnSubmit).off('click').on('click', function(e) {
        e.preventDefault();
        formSubmit(form, callback, modal);
    })
}

export let formSubmit = function(form, callback, modal = null) {
    let callbackdone = function(data) {
        $('#page_content').html(data);
        if(modal) hideModal(modal);
        callback();
    }
    callAjaxForm(form, callbackdone);
}

export let hideModal = function(modal) {
    $(modal).modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

export let copyToClipboardWithId = function() {
    $('#button_copy').on('click', function() {
        let id = $(this).data('id');
        navigator.clipboard.writeText($('#input_link_' + id).val()).then(r  => {
            let linkCopied = $('#link_copied_'+id)
            linkCopied.removeClass('d-none');
            function removeMessage() {
                linkCopied.addClass('d-none');
            }
            setTimeout(removeMessage, 3000);
        });
    })
}

export let showHideSpanIcon = function(element, hasClass, classToAdd, toRemove, toAdd) {
    if ($(element).hasClass(hasClass)) {
        $(element).removeClass(hasClass).addClass(classToAdd)
        $(element).find('i').removeClass(toRemove).addClass(toAdd)
    } else {
        $(element).removeClass(classToAdd).addClass(hasClass)
        $(element).find('i').removeClass(toAdd).addClass(toRemove)
    }
}

export let editFileName = function() {
    $('.file-edit').off('click').on('click', function(e) {
        e.preventDefault()
        let input = $(this).data('input')
        let name = $(this).data('name')
        let edit = $(this).data('edit')
        $(input).removeClass('d-none')
        $(name).addClass('d-none')

        $(edit).off('click').on('click', function(e) {
            e.preventDefault()
            let url = $(this).data('href')
            let inputForm = $(this).data('input')
            url += '?name=' + $(inputForm).val()
            let callback = function(response) {
                $(input).addClass('d-none')
                $(name).removeClass('d-none')
                $(name).find('span').html(response)
                $(inputForm).val(response)
            }
            callAjax(url, 'GET', 'JSON', null, callback)
        })
    })
}

export function addItemToFormCollection(button, callbackDelete) {
    $(button).on('click', function(e) {
        const collectionHolder = document.querySelector('.' + e.currentTarget.dataset.collectionHolderClass)
        const item = document.createElement('li')
        item.classList.add('mb-1')
        item.innerHTML = collectionHolder
            .dataset
            .prototype
            .replace(
                /__name__/g,
                collectionHolder.dataset.index
            )

        collectionHolder.appendChild(item)
        collectionHolder.dataset.index++

        if (callbackDelete) {
            callbackDelete()
        }
    })
}
