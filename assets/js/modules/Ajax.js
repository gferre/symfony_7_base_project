import $ from "jquery";
import {hideModal} from "./Utils";
import {showAlert} from "./SweetAlert";

export function initAjax() {
    // INITIALIZE AJAX
    (function addXhrProgressEvent($) {
        let originalXhr = $.ajaxSettings.xhr;
        $.ajaxSetup({
            progress: $.noop,
            xhr: function() {
                let xhr = originalXhr(), that = this;
                if (xhr) {
                    if (typeof xhr.addEventListener === "function") {
                        xhr.addEventListener("progress", function(event) {
                            that.progress(event);

                            if (that.global) {
                                let eventProgress = $.Event('ajaxProgress', event);
                                eventProgress.type = 'ajaxProgress';
                                $(document).trigger(eventProgress, [xhr]);
                            }
                        },false);
                    }
                }
                return xhr;
            }
        });
    })(jQuery);
}

export function callAjax(url,type,dataType,data,callbackDone = null,callbackBefore = null)
{
    if ('#' === url)
        url = null;

    if (null !== url)
    {
        if (callbackBefore) {
            callbackBefore();
        }

        $.ajax({
            url: url,
            type: type,
            data: data,
            dataType: dataType
        })
            .done( function(data)
            {
                if (callbackDone) {
                    callbackDone(data);
                }
            })
            .fail( function (jqXHR)
            {
                console.error(jqXHR.responseText);
            })
        ;
    } else {
        console.error('Url invalide');
    }
}

export function clickOnAjax(button, contentToReplace, callbackdone, alertType, message) {
    $(button).off('click').on('click', function(e) {
        e.preventDefault()
        let url = $(this).attr('href')
        let callback = function(data) {

            $(contentToReplace).html(data)

            if (callbackdone) {
                callbackdone();
            }

            if (alertType && message) {
                showAlert(alertType, message)
            }

        }
        callAjax(url, 'GET', 'HTML', null, callback)
    })
}

export function replaceContentData(content, data)
{
    if(content instanceof Array){
        for(let key in content){
            replaceContentData(content[key], data);
        }
    }else {
        let objData = $(data);
        let newContent;

        if (content.attr('id') === objData.attr('id')) {
            newContent = objData;
        } else {
            newContent = objData.find("#" + content.attr('id'));
            if (newContent.length === 0) {
                content.html(data);
                return;
            }
        }
        content.replaceWith(newContent);
    }
}

export function callAjaxForm(form, callback = null, fields = null, callbackError = null)
{
    let formData = new FormData(form[0]);
    $.each( fields, function(i, value) {
        formData.append(i,value);
    });

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        processData: false,
        contentType: false,
        cache: false,
        dataType: 'html',
        data: formData,
        success: function(data) {
            if (callback) {
                callback(data);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (callbackError) {
                callbackError(xhr.responseText);
            }
            else {
                console.error(xhr.responseText);
                console.error(thrownError);
            }
        }
    });
}

export let callAjaxModalForm = function(modalId,
                                        btns,
                                        callbackshow = null,
                                        submitButton = null,
                                        formId = null,
                                        callbackSubmit = null) {
    let showForms = function(e, btn) {
        e.preventDefault();
        let url = $(btn).attr('href');
        let modal = $('#'+modalId)

        let callback = function(data) {
            $(modal).find('#loader').addClass('d-none');
            $(modal).find('#form_content').removeClass('d-none');
            $(modal).find('#form_content').html(data);

            if(callbackshow) callbackshow();

            let modalElement = document.getElementById(modalId)
            modalElement.addEventListener('hidden.bs.modal', function () {
                $(modal).find('#loader').removeClass('d-none');
                $(modal).find('#form_content').addClass('d-none');
                $(modal).find('#form_content').empty();
            })

            if (submitButton) {
                $(submitButton).off('click').on('click', function(e) {
                    console.log('click')
                    e.preventDefault()
                    hideModal($(modal))
                    callAjaxForm($(formId), callbackSubmit)
                })
            }
        }

        callAjax(url, 'GET', 'HTML', null, callback);
    }

    $(btns).each(function(key, btn) {
        $(btn).off('click').on('click', function(e) { showForms(e, $(this)); })
        $(btn).off('click').on('click', function(e) { showForms(e, $(this));})
    })
}