import Swal from 'sweetalert2'

export let alertTypes = {
    success: 'success',
    error: 'error',
}

export function successAlert(message) {
    showAlert("success", message)
}

export function errorAlert(message = "Une erreur est survenue.") {
    showAlert("error", message)
}

export function showAlert(type = "success", message) {
    Swal.fire({
        position: "top-end",
        icon: type,
        title: message,
        showConfirmButton: false,
        timer: 1500,
        toast: true
    }).then((result) => {
    });
}

export function initClickAlertConfirm(element, title, text, callback) {
    $(element).on('click', function(e) {
        e.preventDefault()
        Swal.fire({
            title: title,
            text: text,
            showDenyButton: true,
            confirmButtonText: "Confirmer",
            denyButtonText: "Annuler",
            icon: 'warning'
        }).then((result) => {
            if (result.isConfirmed) {
                callback(result)
            } else if (result.isDenied) {
                Swal.close()
            }
        });
    })
}

export function initAlertButtonsChoices(element,
                                           title,
                                           text,
                                           buttonOne,
                                           buttonTwo = null,
                                           callbackOne = null,
                                           callbackTwo = null) {
    $(element).off('click').on('click', function(e) {
        e.preventDefault()

        let options = {
            title: title,
            text: text,
            showDenyButton: false,
            showCancelButton: true,
            confirmButtonText: buttonOne,
            denyButtonText: null,
            cancelButtonText: 'Annuler'
        }

        if (buttonTwo) {
            options.showDenyButton = true
            options.denyButtonText = buttonTwo
        }

        Swal.fire(options).then((result) => {
            if (buttonOne && result.isConfirmed && callbackOne) {
                callbackOne($(element))
            }
            if (buttonTwo && result.isDenied && callbackTwo) {
                callbackTwo($(element))
            }
        });
    })
}