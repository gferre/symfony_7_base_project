export default class Filters {

    static init(dataFilters, dataSearchs) {

        let conf = {
            items: '.tr-filter',
            selectFilters: '.select-filter',
            inputFilters: '.input-filter',
            clearFilters: '#clear_filters',
            filtersResult: '#filters_result',
        }


        let currentFilters = [];
        $(dataFilters).each(function(key, data) {
            currentFilters[data] = '0';
        })
        let currentSearchs = [];
        $(dataSearchs).each(function(key, data) {
            currentSearchs[data] = '';
        })

        let filters = function() {
            let filtersNb = 0;
            let all = $(conf.items);
            all.hide();
            all.each(function(key, tr) {
                let trVisibility = [];

                if(dataFilters.length > 0) {
                    $(dataFilters).each(function(key, data) {
                        if(currentFilters[data] == '0') {
                            trVisibility.push(true)
                        } else {
                            trVisibility.push($(tr).data(data) == currentFilters[data])
                        }
                    })
                }

                if(dataSearchs.length > 0) {
                    $(dataSearchs).each(function(key, data) {
                        if(currentSearchs[data] == '') {
                            trVisibility.push(true);
                        } else {
                            trVisibility.push($(tr).data(data).toLowerCase().includes(currentSearchs[data].toLowerCase()))
                        }
                    })
                }

                let checker = arr => arr.every(v => v === true);
                if(checker(trVisibility)) {
                    $(tr).show();
                    filtersNb++;
                }
            })

            if(filtersNb > 0) {
                $(conf.filtersResult).text('('+filtersNb+')');
            }
        }

        $(conf.inputFilters).on('input', function() {
            let filterName = $(this).data('filter');
            currentSearchs[filterName] = $(this).val();
            filters();
        })

        $(conf.selectFilters).on('change', function() {
            let filterName = $(this).data('filter');
            currentFilters[filterName] = $(this).val();
            filters();
        })

        $(conf.clearFilters).off('click').on('click', function() {
            $(conf.items).show();
            if(dataFilters.length > 0) {
                $(dataFilters).each(function(key, data) {
                    currentFilters[data] = '0';
                })
                $(conf.selectFilters).val('0');
            }
            if(dataSearchs.length > 0) {
                $(dataSearchs).each(function(key, data) {
                    currentSearchs[data] = '';
                })
                $(conf.inputFilters).val('');
            }
            $(conf.filtersResult).text('');
        })
    }

}