import {callAjax, initAjax} from "./modules/Ajax";

const $ = require('jquery')
global.$ = global.jQuery = $

import {successAlert} from "./modules/SweetAlert";

$(document).ready(function() {
    initAjax()

    $('#resend_mail_validation').on('click', function(e) {
        e.preventDefault()
        let callback = function(response) {
            if (response === 'OK') {
                successAlert('Le mail de validation a bien été renvoyé !')
            } else {

            }
        }

        callAjax($(this).attr('href'), 'GET', 'JSON', null, callback)
    })

    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

});
