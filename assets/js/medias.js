import D from './modules/Uploader'
$(document).ready(function() {

    let conf = {
        zoneUploader: '#zone_uploader',
        urlUploader: '#url_uploader',
        btnUploader: '#btn_uploader',
    }

    let onUploadSuccess = function(data) {
        console.log(data)
    }

    D.init(
        conf.zoneUploader,
        $(conf.urlUploader).val(),
        onUploadSuccess,
        null,
        false)
})
