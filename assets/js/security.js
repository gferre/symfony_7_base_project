import {checkPasswordValidation, showPassword} from "./modules/PasswordValidation";

$(document).ready(function() {
    showPassword()
    checkPasswordValidation()
})