# Application Name

## Visualisation du projet

## Installation de l'environnement de développement avec Docker

Pré-requis:
- Docker: https://docs.docker.com/engine/install/ubuntu/
- Docker-compose : https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04
- Git : https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git
- NodeJs (min v14), NVM et NPM : https://linuxize.com/post/how-to-install-node-js-on-ubuntu-20-04/

Modifier la variable USER dans le Makefile pour la faire correspondre à l'utilisateur de son environnement et lancer la commande d'installation.
```sh
make install
```

## Makefile

Installation de l'environnement de développement
```sh
make install
```
Entrer dans le container php
```sh
make php
```
Démarrer les containers
```sh
make start
```
Compilation des assets (dev)
```sh
make assets-dev
```
Compilation des assets (prod)
```sh
make assets-prod
```
Compalitation des assets en temps réel (dev)
```sh
make assets-watch
```
Update des librairies back-end
```sh
make composer-update
```
Migration de la base de données
```sh
make doctrine-migrate
```
Vider le cache de Symfony (dev)
```sh
make cc
```
Mise à jour du cache gitignore après une update
```sh
make update-gitinore
```
